#: title  : moss/xelatex
#: author : "Willian Paixao" <willian@ufpa.br>
#: version: "1.1.0"
FROM ubuntu:20.04
MAINTAINER songrit@npu.ac.th

LABEL version="1.2.2"

ENV DEBIAN_FRONTEND noninteractive

# Install all TeX and LaTeX dependences
RUN apt-get update && \
  apt-get install --yes --no-install-recommends \
  git \
  ca-certificates \
  inotify-tools \
  lmodern \
  git \
  make \
  gnuplot \
  wget \
  texlive-fonts-recommended \
  texlive-publishers \
  texlive-fonts-extra \
  texlive-pstricks \ 
  texlive-pictures \
  texlive-metapost \
  texlive-music \
  texlive-xetex  \
  texlive-luatex \
  texlive-games \
  texlive-humanities \
  texlive-full \
  texlive-science && \
  apt-get autoclean && apt-get --purge --yes autoremove && \
  git config --global http.postBuffer 1048576000 && \
  wget https://github.com/jeffmcneill/thai-font-collection/archive/1.01.tar.gz && \
  tar xf 1.01.tar.gz && \
  rm 1.01.tar.gz && \
  git clone https://github.com/anoochit/suriyan && \
  mkdir -p ~/.fonts && \
  cp -r suriyan/extension-pack4ubuntu/fonts/ttf/thaifont-abc/* ~/.fonts/ && \
  find thai-font-collection-1.01 -name '*.ttf'  -exec cp {} ~/.fonts/ \; && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 

# Export the output data
WORKDIR /data
VOLUME ["/data"]

